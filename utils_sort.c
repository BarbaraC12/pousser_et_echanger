#include "includes/push_swap.h"

int	medium_list(t_list *stack)
{
	t_list		*index;
	int			size;
	long long	i;

	i = 0;
	size = 0;
	index = stack;
	while (index && (index->e_arg != SORTED && index->e_arg != PIVOT))
	{
		i += *(int *)(index->content);
		size++;
		index = index->next;
	}
	if (size)
		i = i / size;
	return (i);
}

t_list	*find_maximum_sort(t_list *stack_src)
{
	t_list	*is_max;

	is_max = stack_src;
	if (!stack_src)
		return (NULL);
	while (stack_src && stack_src->e_arg == RANDOM)
	{
		if (*(int *)(stack_src->content) > *(int *)(is_max->content))
			is_max = stack_src;
		stack_src = stack_src->next;
	}
	return (is_max);
}

t_list	*find_minimum_sort(t_list *stack_src)
{
	t_list	*is_min;

	is_min = stack_src;
	if (!stack_src)
		return (NULL);
	while (stack_src && stack_src->e_arg == RANDOM)
	{
		if (*(int *)(stack_src->content) < *(int *)(is_min->content))
			is_min = stack_src;
		stack_src = stack_src->next;
	}
	return (is_min);
}

int	is_sort(t_list *list)
{
	while (list->next)
	{
		if (*(int *)(list->content) > *(int *)(list->next->content))
			return (0);
		list = list->next;
	}
	return (1);
}

int	is_reverse_sort(t_list *list)
{
	while (list->next)
	{
		if (*(int *)(list->content) < *(int *)(list->next->content))
			return (0);
		list = list->next;
	}
	return (1);
}
