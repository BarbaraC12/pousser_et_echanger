#include "includes/push_swap.h"

t_list	*find_maximum(t_list *stack_src)
{
	t_list	*is_max;

	is_max = stack_src;
	if (!stack_src)
		return (NULL);
	while (stack_src)
	{
		if (*(int *)(stack_src->content) > *(int *)(is_max->content))
			is_max = stack_src;
		stack_src = stack_src->next;
	}
	return (is_max);
}

t_list	*find_minimum(t_list *stack_src)
{
	t_list	*is_min;

	is_min = stack_src;
	if (!stack_src)
		return (NULL);
	while (stack_src)
	{
		if (*(int *)(stack_src->content) < *(int *)(is_min->content))
			is_min = stack_src;
		stack_src = stack_src->next;
	}
	return (is_min);
}
