#include "includes/push_swap.h"

void	push_swap(t_list **stack_a, t_list **stack_b)
{
	while (!(is_sort(*stack_a)))
	{
		if (ft_lstsize(*stack_a) <= 6)
		{
			particular_sort(stack_a, stack_b);
			return ;
		}
		while (*stack_a && (*stack_a)->e_arg != SORTED)
			sort_a(stack_a, stack_b);
		while (*stack_b)
			push_a(stack_a, stack_b, 1);
	}
}
