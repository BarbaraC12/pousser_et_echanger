#include "includes/push_swap.h"

t_list	*find_middle(t_list *stack)
{
	t_list	*pivot;
	t_mid	piv;

	piv.max = medium_list(stack);
	piv.min = *(int *)(find_minimum_sort(stack)->content);
	pivot = element_bt_two_val(stack, piv.min, piv.max);
	piv.intervall = average_intervall(stack);
	while (!pivot || (pivot && low_or_great_mid(stack, pivot)))
	{
		while ((!pivot || (pivot && low_or_great_mid(stack, pivot) == 1)))
		{
			search_midline(&piv, stack, &pivot, '>');
		}
		piv.min = *(int *)(find_minimum_sort(stack)->content);
		while ((!pivot || (pivot && low_or_great_mid(stack, pivot) == -1)))
		{
			search_midline(&piv, stack, &pivot, '<');
		}
		piv.intervall = piv.intervall / 2;
	}
	pivot->e_arg = PIVOT;
	return (pivot);
}

void	search_midline(t_mid *piv, t_list *stack, t_list **pivot, char c)
{
	if (c == '>')
	{
		piv->min = piv->max;
		piv->max += piv->intervall;
		if (piv->max >= *(int *)(find_maximum_sort(stack)->content) ||
			piv->max <= *(int *)(find_minimum_sort(stack)->content))
		{
			piv->min -= piv->intervall;
			piv->max -= piv->intervall;
			piv->intervall = piv->intervall / 2;
		}
		*pivot = element_bt_two_val(stack, piv->min, piv->max);
	}
	if (c == '<')
	{
		piv->max -= piv->intervall;
		piv->intervall = piv->intervall;
		if (piv->max <= piv->min)
		{
			piv->max += piv->intervall;
			piv->intervall = piv->intervall / 2;
		}
		*pivot = element_bt_two_val(stack, piv->min, piv->max);
	}
}

int	average_intervall(t_list *stack)
{
	long long	intervall_sum;
	int			intervall;
	int			size;

	intervall_sum = 0;
	size = 0;
	while (stack && stack->next && (stack->e_arg != SORTED
			&& stack->e_arg != PIVOT))
	{
		intervall = ft_val_abs(*(int *)(stack->content)
				- *(int *)(stack->next->content));
		intervall_sum += intervall;
		size++;
		stack = stack->next;
	}
	intervall_sum = intervall_sum / size;
	return (intervall_sum);
}

int	low_or_great_mid(t_list *stack, t_list *pivot_tested)
{
	int	low;
	int	great;

	low = 0;
	great = 0;
	while (stack && stack->e_arg == RANDOM)
	{
		if (*(int *)(stack->content) > *(int *)(pivot_tested->content))
			great += 1;
		else if (*(int *)(stack->content) < *(int *)(pivot_tested->content))
			low += 1;
		stack = stack->next;
	}
	if (ft_val_abs(great - low) < 2)
		return (0);
	else if (great - low > 0)
		return (1);
	else if (great - low < 0)
		return (-1);
	return (0);
}

t_list	*element_bt_two_val(t_list *stack, long min, long max)
{
	t_list	*elem;

	elem = NULL;
	while (stack && (stack->e_arg == RANDOM))
	{
		if (*(int *)(stack->content) <= max && *(int *)(stack->content) >= min)
		{
			elem = stack;
			min = *(int *)(elem->content);
		}
		stack = stack->next;
	}
	return (elem);
}
