#include "includes/push_swap.h"

void	clean_up_sort(t_list **stack_a, t_list **stack_b, int to_clean, char c)
{
	if (c == 'A')
	{
		while (to_clean)
		{
			rev_rotate_a(stack_a, 1);
			if ((*stack_a)->e_arg == PIVOT)
				push_b(stack_a, stack_b, 1);
			to_clean--;
		}
		if ((*stack_b)->e_arg == PIVOT)
			push_a(stack_a, stack_b, 1);
	}
	if (c == 'B')
	{
		while (to_clean)
		{
			rev_rotate_b(stack_b, 1);
			if ((*stack_b)->e_arg == PIVOT)
				push_a(stack_a, stack_b, 1);
			to_clean--;
		}
	}
}

int	to_clean(t_list *stack)
{
	int	count_pivot;

	count_pivot = 0;
	while (stack)
	{
		if (stack->e_arg == SORTED)
			return (1);
		if (stack->e_arg == PIVOT)
			count_pivot++;
		if (count_pivot > 1)
			return (1);
		stack = stack->next;
	}
	return (0);
}

int	mid_dispatch(t_list *stack, t_list *pivot, char c)
{
	if (c == 'A')
	{
		while (stack)
		{
			if (*(int *)(stack->content) < *(int *)(pivot->content))
				return (0);
			stack = stack->next;
		}
	}
	else if (c == 'B')
	{
		while (stack)
		{
			if (*(int *)(stack->content) > *(int *)(pivot->content))
				return (0);
			stack = stack->next;
		}
	}
	return (1);
}
