#include "../includes/push_swap.h"

void	rotate(t_list **stack)
{
	void	*tmp;

	if (!*stack || !(*stack)->next)
		return ;
	tmp = *stack;
	*stack = (*stack)->next;
	ft_lstadd_back(stack, tmp);
}

void	rotate_a(t_list **stack_a, int print)
{
	rotate(stack_a);
	if (print)
		ft_putstr("ra\n");
}

void	rotate_b(t_list **stack_b, int print)
{
	rotate(stack_b);
	if (print)
		ft_putstr("rb\n");
}

void	rotate_r(t_list **stack_a, t_list **stack_b, int print)
{
	rotate(stack_a);
	rotate(stack_b);
	if (print)
		ft_putstr("rr\n");
}
