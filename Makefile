NAME		= push_swap

SRCS		= main.c push_swap.c stack_dispatch.c stack_particular_sort.c \
				stack_sort_a.c stack_sort_b.c utils_clean.c utils_sort.c\
				utils_mid_function.c utils_particular_sort.c \
				utl_swap.c utl_push.c utl_rev_rotate.c utl_rotate.c

OBJS_PATH	= ./objs/
TMP			= ${SRCS:.c=.o}
OBJS		= $(addprefix ${OBJS_PATH},${TMP})

HEADER			= includes

CC			= gcc
CFLAGS 		= -Wall -Wextra -Werror -g3 -fsanitize=address

$(NAME):	${OBJS}
	echo "\033[1;92m\t>>> Compilation <<<\033[0m"
	make -C libft all
	mv libft/libft.a . 
	ar -rc ${NAME} ${OBJS}
	${CC} ${CFLAGS} ${OBJS} libft.a -o ${NAME}
	echo "\033[1;93m>>>> Compilation complet\033[0m"

${OBJS_PATH}%.o: %.c
	mkdir -p ${OBJS_PATH}
	${CC} -c ${CFLAGS} -o $@ $< -I ${HEADER}

all:		${NAME}

clean:
	rm -rf ${OBJS_PATH}
	make -C libft clean
	echo "\033[1;93m>>> Object file clean\033[0m"

fclean:		
	make clean
	rm -rf ${NAME} libft.a
	echo "\033[1;93m>>>> Project clean\033[0m"

re:			fclean all
			touch ~/.reset

valgrind: all
	make clean
	ARG="4 67 -3 87 23"; valgrind ./push_swap $ARG

.PHONY: 	all fclean clean re valgrind bcano
