#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <limits.h>
# include "../libft/libft.h"

typedef struct s_mid
{
	long	min;
	long	max;
	long	intervall;
}				t_mid;

void	push_swap(t_list **stack_a, t_list **stack_b);

void	swap(t_list *stack);
void	swap_a(t_list **stack_a, int print);
void	swap_b(t_list **stack_b, int print);
void	swap_s(t_list **stack_a, t_list **stack_b, int print);

void	push(t_list **stack_src, t_list **stack_dst);
void	push_a(t_list **stack_a, t_list **stack_b, int print);
void	push_b(t_list **stack_b, t_list **stack_a, int print);

void	rotate(t_list **stack);
void	rotate_a(t_list **stack_a, int print);
void	rotate_b(t_list **stack_b, int print);
void	rotate_r(t_list **stack_a, t_list **stack_b, int print);

void	rev_rotate(t_list **stack);
void	rev_rotate_a(t_list **stack_a, int print);
void	rev_rotate_b(t_list **stack_b, int print);
void	rev_rotate_r(t_list **stack_a, t_list **stack_b, int print);

t_list	*init_element(void *content);

void	sort_a(t_list **stack_a, t_list **stack_b);
void	sort_b(t_list **stack_a, t_list **stack_b);
void	insert_sort_a(t_list **stack_a, t_list **stack_b);
void	insert_sort_b(t_list **stack_a, t_list **stack_b);
void	dispatch_a(t_list **stack_a, t_list **stack_b, int treat, int clean);
void	dispatch_b(t_list **stack_a, t_list **stack_b, int treat, int clean);

void	trio_sort(t_list **stack_src);
void	small_sort(t_list **stack_a, t_list **stack_b);
void	particular_sort(t_list **stack_a, t_list **stack_b);

t_list	*find_maximum_sort(t_list *stack_src);
t_list	*find_minimum_sort(t_list *stack_src);
t_list	*find_maximum(t_list *stack_src);
t_list	*find_minimum(t_list *stack_src);
int		medium_list(t_list *stack);
int		is_reverse_sort(t_list *list);
int		is_sort(t_list *list);

void	search_midline(t_mid *piv, t_list *stack, t_list **pivot, char c);
t_list	*find_middle(t_list *stack);
t_list	*element_bt_two_val(t_list *stack, long min, long max);
int		average_intervall(t_list *stack);
int		low_or_great_mid(t_list *stack, t_list *pivot_tested);

void	push_front(t_list **stack_a, t_list **stack_b, t_list *elem, char c);
int		mid_low_place(t_list *stack, t_list *elem);
int		ascend_part(t_list *list);
int		descend_part(t_list *list);
int		sort_distrib(t_list **stack_a, t_list **stack_b, char name_stack);

void	clean_up_sort(t_list **stack_a, t_list **stack_b, int clear, char c);
int		to_clean(t_list *stack);
int		mid_dispatch(t_list *stack, t_list *pivot, char c);

#endif //Project made by bcano to 42school and grade 93/100 
