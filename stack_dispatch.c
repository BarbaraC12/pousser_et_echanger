#include "includes/push_swap.h"

void	push_front(t_list **stack_a, t_list **stack_b, t_list *elem, char c)
{
	if (c == 'A')
	{
		if (mid_low_place(*stack_a, elem))
			while (*stack_a != elem)
				rev_rotate_a(stack_a, 1);
		else
			while (*stack_a != elem)
				rotate_a(stack_a, 1);
	}
	else if (c == 'B')
	{
		if (mid_low_place(*stack_b, elem))
			while ((*stack_b) != elem)
				rev_rotate_b(stack_b, 1);
		else
			while ((*stack_b) != elem)
				rotate_b(stack_b, 1);
	}
}

int	mid_low_place(t_list *stack, t_list *elem)
{
	int	mid_size;
	int	i;

	i = 0;
	mid_size = ft_lstsize(stack) / 2;
	while (stack)
	{
		if (stack == elem)
			break ;
		stack = stack->next;
		i++;
	}
	if (i > mid_size)
		return (1);
	return (0);
}

int	ascend_part(t_list *list)
{
	while (list->next && list->next->e_arg != PIVOT)
	{
		if (*(int *)(list->content) > *(int *)(list->next->content))
			return (0);
		list = list->next;
	}
	return (1);
}

int	descend_part(t_list *list)
{
	while (list->next && list->next->e_arg != SORTED)
	{
		if (*(int *)(list->content) < *(int *)(list->next->content))
			return (0);
		list = list->next;
	}
	return (1);
}

int	sort_distrib(t_list **stack_a, t_list **stack_b, char name_stack)
{
	if (name_stack == 'a')
	{
		if (ascend_part(*stack_a))
		{
			while ((*stack_a) && (*stack_a)->e_arg != PIVOT)
			{
				push_b(stack_a, stack_b, 1);
				(*stack_b)->e_arg = SORTED;
			}
			return (1);
		}
	}
	if (name_stack == 'b')
	{
		if (descend_part(*stack_b))
		{
			(*stack_b)->e_arg = SORTED;
			return (1);
		}
	}
	return (0);
}
