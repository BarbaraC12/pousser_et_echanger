#include "includes/push_swap.h"

void	trio_sort(t_list **stack_src)
{
	t_list	*is_max;

	is_max = find_maximum(*stack_src);
	if (is_max->next && is_max->next->next)
		rotate_a(stack_src, 1);
	else if (is_max->next && !is_max->next->next)
		rev_rotate_a(stack_src, 1);
	if (!(is_sort(*stack_src)))
		swap_a(stack_src, 1);
	(*stack_src)->e_arg = SORTED;
}

void	small_sort(t_list **stack_a, t_list **stack_b)
{
	t_list	*is_min;

	while (sort_size(*stack_a) > 3)
	{
		is_min = find_minimum(*stack_a);
		while ((*stack_a) != is_min && !(is_sort(*stack_a)))
		{
			if (!is_min->next || !is_min->next->next)
				rev_rotate_a(stack_a, 1);
			else if ((*stack_a)->next == is_min)
				swap_a(stack_a, 1);
			else
				rotate_a(stack_a, 1);
			is_min = find_minimum(*stack_a);
		}
		push_b(stack_a, stack_b, 1);
	}
	trio_sort(stack_a);
	while (*stack_b)
		push_a(stack_a, stack_b, 1);
}

void	particular_sort(t_list **stack_a, t_list **stack_b)
{
	if (ft_lstsize(*stack_a) == 2)
	{
		if (!(is_sort(*stack_a)))
			swap_a(stack_a, 1);
	}
	if (ft_lstsize(*stack_a) == 3)
		trio_sort(stack_a);
	else if (sort_size(*stack_a) <= 6)
		small_sort(stack_a, stack_b);
}
