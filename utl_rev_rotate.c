#include "../includes/push_swap.h"

void	rev_rotate(t_list **stack)
{
	void	*tmp;
	t_list	*elem;

	if (!*stack || !(*stack)->next)
		return ;
	elem = *stack;
	if (!elem->next->next)
	{
		swap(*stack);
		return ;
	}
	while (elem->next->next)
		elem = elem->next;
	tmp = elem->next;
	elem->next = NULL;
	ft_lstadd_front(stack, tmp);
}

void	rev_rotate_a(t_list **stack_a, int print)
{
	rev_rotate(stack_a);
	if (print)
		ft_putstr("rra\n");
}

void	rev_rotate_b(t_list **stack_b, int print)
{
	rev_rotate(stack_b);
	if (print)
		ft_putstr("rrb\n");
}

void	rev_rotate_r(t_list **stack_a, t_list **stack_b, int print)
{
	rev_rotate(stack_a);
	rev_rotate(stack_b);
	if (print)
		ft_putstr("rrr\n");
}
