# Push_n_swap V.4

Because Swap_push isn’t as natural

This project will make you sort data on a stack, with a limited set of instructions, using
the lowest possible number of actions. To succeed you’ll have to manipulate various
types of algorithms and choose the one (of many) most appropriate solution for an
optimized data sorting

Grade : 93 ✅

## Actions

- swap (s_): Invert the two first elements from the stack
- push (p_): Send the first element of the opposite stack and place at the top
- rotate (r_): Moves all items on the stack one position top
- reverse rotate (rr_): Moves all items on the stack one position down

> a -> stack a | b -> stack b | r -> a & b same time 

## Rating

- 3 values : 2-3 actions
- 5 values : Max 12 actions
- 100 values : 
  - 1pts -> < 1500  
  - 2pts -> < 1300
  - 3pts -> < 1100
  - 4pts -> < 900 ✅
  - 5pts -> < 700
- 500 values : 
  - 1pts -> < 11500  
  - 2pts -> < 10000
  - 3pts -> < 8500 ✅
  - 4pts -> < 7000
  - 5pts -> < 5500

## Usage

```sh
make
bash
ARG="4 67 3 87 23"; ./push_swap $ARG | wc -l
```
