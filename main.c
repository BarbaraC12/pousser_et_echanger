#include "includes/push_swap.h"

static int	already_exist(t_list *stack)
{
	t_list	*tmp;

	tmp = stack;
	while (tmp->next)
	{
		stack = tmp;
		while (stack->next)
		{
			stack = stack->next;
			if (*(int *)tmp->content == *(int *)stack->content)
				return (1);
		}
		tmp = tmp->next;
	}
	return (0);
}

static int	detect_error(t_list **stack, int argc, char **argv)
{
	int		*nbr;
	int		i;

	i = 1;
	while (i < argc)
	{
		if (!(str_isdigit(argv[i])))
		{
			ft_putstr("Error (not digit)\n");
			exit(ft_lstfree(*stack));
		}
		nbr = malloc(sizeof(int) * 1);
		if (nbr == NULL)
			return (-1);
		*nbr = ft_atoi(argv[i]);
		ft_lstpushback(stack, nbr);
		i++;
	}
	if (already_exist(*stack))
	{
		ft_putstr("Error (duplicat)\n");
		exit(ft_lstfree(*stack));
	}
	return (0);
}

int	main(int argc, char *argv[])
{
	t_list	*stack_a;
	t_list	*stack_b;

	stack_a = NULL;
	stack_b = NULL;
	if (argc < 2)
		return (0);
	detect_error(&stack_a, argc, argv);
	push_swap(&stack_a, &stack_b);
	ft_lstfree(stack_a);
	return (0);
}
