#include "includes/push_swap.h"

void	dispatch_a(t_list **stack_a, t_list **stack_b, int treat, int clean)
{	
	t_list	*pivot;

	pivot = find_middle(*stack_a);
	while (!mid_dispatch(*stack_a, pivot, 'A') || !treat)
	{
		if ((*stack_a)->e_arg == PIVOT)
			treat = 1;
		if (*(int *)(*stack_a)->content < *(int *)pivot->content)
			push_b(stack_a, stack_b, 1);
		else
		{
			rotate_a(stack_a, 1);
			clean++;
		}
	}
	if (to_clean(*stack_a))
		clean_up_sort(stack_a, stack_b, clean, 'A');
	else
		push_front(stack_a, stack_b, pivot, 'A');
}

void	insert_sort_a(t_list **stack_a, t_list **stack_b)
{
	while (*stack_a && ((*stack_a)->e_arg != PIVOT || ((*stack_a)->e_arg
				== PIVOT && (*stack_a) != find_minimum(*stack_a))))
	{
		while ((*stack_a != find_minimum(*stack_a)
				&& ((*stack_a)->e_arg != PIVOT))
			|| ((*stack_a)->e_arg == PIVOT && (*stack_a)
				!= find_minimum(*stack_a)))
		{
			if (*stack_a && (*stack_a)->next && (*stack_a)->next
				== find_minimum(*stack_a))
				swap_a(stack_a, 1);
			else
				push_front(stack_a, stack_b, find_minimum(*stack_a), 'A');
		}
		push_b(stack_a, stack_b, 1);
	}
	(*stack_b)->e_arg = SORTED;
}

void	sort_a(t_list **stack_a, t_list **stack_b)
{
	int	need_clean;
	int	pivot_treat;

	need_clean = 0;
	pivot_treat = 0;
	while (*stack_a && (*stack_a)->e_arg == PIVOT)
	{
		push_b(stack_a, stack_b, 1);
		(*stack_b)->e_arg = SORTED;
	}
	if (!*stack_a)
		return ;
	if (sort_distrib(stack_a, stack_b, 'a'))
		return ;
	if (sort_size(*stack_a) <= 20)
		return (insert_sort_a(stack_a, stack_b));
	dispatch_a(stack_a, stack_b, pivot_treat, need_clean);
	while ((*stack_b) && (*stack_b)->e_arg != SORTED)
		sort_b(stack_a, stack_b);
}
