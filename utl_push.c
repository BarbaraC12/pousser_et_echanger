#include "../includes/push_swap.h"

void	push(t_list **stack_src, t_list **stack_dst)
{
	void	*tmp;

	if (!stack_src)
		return ;
	tmp = *stack_src;
	if ((*stack_src)->next)
		*stack_src = (*stack_src)->next;
	else
		*stack_src = NULL;
	ft_lstadd_front(stack_dst, tmp);
}

void	push_a(t_list **stack_a, t_list **stack_b, int print)
{
	push(stack_b, stack_a);
	if (print)
		ft_putstr("pa\n");
}

void	push_b(t_list **stack_a, t_list **stack_b, int print)
{
	push(stack_a, stack_b);
	if (print)
		ft_putstr("pb\n");
}
