#include "../includes/push_swap.h"

void	swap(t_list *stack)
{
	void	*tmp;

	if (!stack || !stack->next)
		return ;
	tmp = stack->content;
	stack->content = stack->next->content;
	stack->next->content = tmp;
}

void	swap_a(t_list **stack_a, int print)
{
	swap(*stack_a);
	if (print)
		write(1, "sa\n", 3);
}

void	swap_b(t_list **stack_b, int print)
{
	swap(*stack_b);
	if (print)
		write(1, "sb\n", 3);
}

void	swap_s(t_list **stack_a, t_list **stack_b, int print)
{
	swap(*stack_b);
	swap(*stack_a);
	if (print)
		write(1, "ss\n", 3);
}
