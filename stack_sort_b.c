#include "includes/push_swap.h"

void	dispatch_b(t_list **stack_a, t_list **stack_b, int treat, int clean)
{	
	t_list	*pivot;

	pivot = find_middle(*stack_b);
	while (!mid_dispatch(*stack_b, pivot, 'B') || !treat)
	{
		if ((*stack_b)->e_arg == PIVOT)
			treat = 1;
		if (*(int *)(*stack_b)->content > *(int *)pivot->content)
			push_a(stack_a, stack_b, 1);
		else
		{
			rotate_b(stack_b, 1);
			clean++;
		}
	}
	if ((*stack_b)->next)
	{
		if (to_clean(*stack_b))
			clean_up_sort(stack_a, stack_b, clean, 'B');
		else
		{
			push_front(stack_a, stack_b, pivot, 'B');
			push_a(stack_a, stack_b, 1);
		}
	}
}

void	insert_sort_b(t_list **stack_a, t_list **stack_b)
{
	while (*stack_b && ((*stack_b)->e_arg != SORTED || ((*stack_b)->e_arg
				== SORTED && (*stack_b) != find_maximum(*stack_b))))
	{
		while ((*stack_b != find_maximum(*stack_b) && ((*stack_b)->e_arg
					!= SORTED)) || ((*stack_b)->e_arg == SORTED && (*stack_b)
				!= find_maximum(*stack_b)))
		{
			if (*stack_b && (*stack_b)->next && (*stack_b)->next
				== find_maximum(*stack_b))
				swap_b(stack_b, 1);
			else
				push_front(stack_a, stack_b, find_maximum(*stack_b), 'B');
		}
		push_a(stack_a, stack_b, 1);
	}
	(*stack_a)->e_arg = PIVOT;
}

void	sort_b(t_list **stack_a, t_list **stack_b)
{
	int		to_clean;
	int		treat_mid;

	to_clean = 0;
	treat_mid = 0;
	if (sort_distrib(stack_a, stack_b, 'b'))
		return ;
	if (sort_size(*stack_b) <= 10)
		return (insert_sort_b(stack_a, stack_b));
	dispatch_b(stack_a, stack_b, treat_mid, to_clean);
	if (!(*stack_b)->next || ((*stack_b)->next
			&& (*stack_b)->next->e_arg == SORTED))
		(*stack_b)->e_arg = SORTED;
}
